#pragma once
#include "raylib.h"
//-- Screen
const int screenWidth = 1280;
const int screenHeight = 720;
//-- Game
//Player
const float moveSpeed = 8.0f;
const int playerWide = 30;
const int playerTall = 100;
//Ball
const int ballRadius = 20;

void PrintWindow();

void MainMenu();
void GameLoop();

void InitializeGame();
void PrintObjects();
void UpStage();
void SetPaused(bool);
void RestartBall();
bool IsBallColliding(Vector2);
void UpdateBall();
bool IsPlayerOOB(int, int);
void Controller();