#include "Screen.h"
#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
using namespace std;

enum class STATUS {
    MAIN_MENU = 1,
    SETTINGS,
    IN_GAME,
    QUIT
};

struct PLAYER
{
    int positionX = 0;
    int positionY = 0;
};

STATUS currentStatus = STATUS::MAIN_MENU;

// Menu
bool quitGame = false;
int selection = 1;

//--

// Juego
PLAYER ply1;
PLAYER ply2;

float bSpeedX = 6.0f;
float bSpeedY = 5.0f;

Vector2 ballPosition = { screenWidth / 2, screenHeight / 2 };
Vector2 ballSpeed = { bSpeedX, bSpeedY };

bool pause = false;
int score = 0;
int losses = 0;
//--
void PrintWindow() {

    // Initialization
    //--------------------------------------------------------------------------------------

    InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");

    SetTargetFPS(60);               // Set our game to run at 60 frames-per-second

    InitializeGame();
    //--------------------------------------------------------------------------------------

    // Main game loop
    while (!WindowShouldClose() && !quitGame)    // Detect window close button or ESC key
    {
        switch (currentStatus)
        {
        case STATUS::MAIN_MENU:
            MainMenu();
            break;
        case STATUS::SETTINGS:
            break;
        case STATUS::IN_GAME:
            GameLoop();
            break;
        case STATUS::QUIT:
            quitGame = true;
            break;
        default:
            break;
        }
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

}

void MainMenu() {

    // Update
    //----------------------------------------------------------------------------------
    if (IsKeyDown(KEY_UP) && selection > 1) selection--;
    if (IsKeyDown(KEY_DOWN) && selection < 2) selection++;

    if (IsKeyDown(KEY_ENTER)) {
        switch (selection)
        {
        case 1:
            currentStatus = STATUS::IN_GAME;
            InitializeGame();
            pause = true;
            break;
        case 2:
            currentStatus = STATUS::QUIT;
            break;
        default:
            break;
        }
    }
    //----------------------------------------------------------------------------------

    // Draw
    //----------------------------------------------------------------------------------
    BeginDrawing();

    ClearBackground(LIGHTGRAY);

    DrawText("Some Pong Clone", screenWidth * 0.27, screenHeight * 0.3, screenHeight * 0.1, DARKGRAY);

    if (selection == 1) {
        DrawText("Play", screenWidth * 0.46, screenHeight * 0.45, screenHeight * 0.07, YELLOW);
        DrawText("Quit", screenWidth * 0.46, screenHeight * 0.55, screenHeight * 0.07, WHITE);
    }
    else {
        DrawText("Play", screenWidth * 0.46, screenHeight * 0.45, screenHeight * 0.07, WHITE);
        DrawText("Quit", screenWidth * 0.46, screenHeight * 0.55, screenHeight * 0.07, YELLOW);
    }

    DrawText("[UP] and [DOWN] to select, [ENTER] to confirm option", screenWidth * 0.23, screenHeight * 0.95, screenHeight * 0.04, DARKGRAY);
    EndDrawing();
    //----------------------------------------------------------------------------------

}

void GameLoop() {

    // Update
    //----------------------------------------------------------------------------------
    Controller();
    if (!pause) {
        UpdateBall();
        UpStage();
    }
    //----------------------------------------------------------------------------------

    // Draw
    //----------------------------------------------------------------------------------
    BeginDrawing();

    ClearBackground(DARKGRAY);

    PrintObjects();

    EndDrawing();
    //----------------------------------------------------------------------------------

}

void InitializeGame() {

    //Players
    ply1.positionY = (screenHeight / 2) - (playerTall / 2);
    ply2.positionY = (screenHeight / 2) - (playerTall / 2);
    ply1.positionX = screenWidth * 0.1;
    ply2.positionX = screenWidth * 0.9;

    //Ball
    ballPosition = { screenWidth / 2, screenHeight / 2 };
    ballSpeed = { bSpeedX, bSpeedY };

    //Game
    score = 0;
    losses = 0;

}

void PrintObjects() {

    DrawText(TextFormat("Score: %03i", score), screenWidth * 0.4, screenHeight * 0.05, screenHeight * 0.07, LIGHTGRAY);
    DrawText(TextFormat("Misses: %02i", losses), screenWidth * 0.42, screenHeight * 0.15, screenHeight * 0.06, LIGHTGRAY);

    DrawRectangle(ply1.positionX, ply1.positionY, playerWide, playerTall, MAROON);
    DrawRectangle(ply2.positionX * 0.95 + playerWide, ply2.positionY, playerWide, playerTall, DARKBLUE);
    if (!pause) {
        DrawCircleV(ballPosition, ballRadius, ORANGE);
    }

    if (pause) {
        DrawText("Game Paused", screenWidth * 0.35, screenHeight * 0.45, screenHeight * 0.08, LIGHTGRAY);
        DrawText("Press ENTER to resume", screenWidth * 0.36, screenHeight * 0.75, screenHeight * 0.04, LIGHTGRAY);
        DrawText("Press M to return to Main Menu", screenWidth * 0.32, screenHeight * 0.85, screenHeight * 0.04, LIGHTGRAY);
        DrawText("Press ESC to close the game", screenWidth * 0.34, screenHeight * 0.95, screenHeight * 0.04, LIGHTGRAY);
    }
}

void UpStage() {

    switch (score)
    {
    case 5:
        ballSpeed.x = ballSpeed.x * 1.25;
        score++;
        break;
    case 12:
        ballSpeed.x = ballSpeed.x * 1.25;
        score++;
        break;
    case 24:
        ballSpeed.x = ballSpeed.x * 1.25;
        score++;
        break;
    case 48:
        ballSpeed.x = ballSpeed.x * 1.25;
        score++;
        break;
    default:
        break;
    }

    if (losses > 10) {

    }

}

void SetPaused(bool paused) {
    pause = paused;
}

void RestartBall() {

    ballPosition = { screenWidth / 2, screenHeight / 2 };
    ballSpeed.x *= -1.0f;
    ballSpeed.y *= -1.0f;
    SetPaused(true);
}

void BallCollision() {

    //Wall Collisions
    if ((ballPosition.x >= (screenWidth - ballRadius)) || (ballPosition.x <= ballRadius)) {
        RestartBall();
        losses++;
    }
    if ((ballPosition.y >= (screenHeight - ballRadius)) || (ballPosition.y <= ballRadius)) ballSpeed.y *= -1.0f;

    //Player Collisions
    if (ballSpeed.x < 0) { // Player 1 Direction

        if ((ballPosition.y >= (ply1.positionY - (ballRadius/2))) && (ballPosition.y <= (ply1.positionY + playerTall) + (ballRadius/2) ) && (ballPosition.x <= ((ply1.positionX + playerWide) + ballRadius)) && (ballPosition.x >= (ply1.positionX - playerWide)) ) {
            if (ballPosition.x - ballRadius < ply1.positionX + (playerWide - 10)) {// Si la pelota se pasa del jugador.
                if ( ballPosition.y < ply1.positionY + (playerTall/2) ) {// Si choca de un lado o otro.
                    ballSpeed.y = -bSpeedY;
                    ballSpeed.x *= -1.0f;
                }
                else {
                    ballSpeed.y = bSpeedY;
                    ballSpeed.x *= -1.0f;
                }
            }
            else {
                ballSpeed.x *= -1.0f;
            }
            score++;
        }

    }
    else { // Player 2 Direction

        if ((ballPosition.y >= (ply2.positionY - (ballRadius/2))) && (ballPosition.y <= (ply2.positionY + playerTall) + (ballRadius/2) ) && (ballPosition.x >= ((ply2.positionX - playerWide) - ballRadius)) && (ballPosition.x <= (ply2.positionX + playerWide)) ) {
            if (ballPosition.x + ballRadius > ply2.positionX + 10 ) {// Si la pelota se pasa del jugador.
                if (ballPosition.y < ply2.positionY + (playerTall/2)) {// Si choca de un lado o otro.
                    ballSpeed.y = -bSpeedY;
                    ballSpeed.x *= -1.0f;
                }
                else {
                    ballSpeed.y = bSpeedY;
                    ballSpeed.x *= -1.0f;
                }
            }
            else {
                ballSpeed.x *= -1.0f;
            }
            score++;
        }

    }

}

void UpdateBall() {
    ballPosition.x += ballSpeed.x;
    ballPosition.y += ballSpeed.y;

    BallCollision();
}

bool IsPlayerOOB(int pos, int bounds) {

    if (pos + bounds >= screenHeight || pos <= 0) {
        return true;
    }
    else {
        return false;
    }

}

void Controller() {

    if (!pause) {
        int posToMove = 0;
        // Update
        //----------------------------------------------------------------------------------
        //Player1
        posToMove = ply1.positionY;
        if (IsKeyDown(KEY_W)) posToMove -= moveSpeed;
        if (IsKeyDown(KEY_S)) posToMove += moveSpeed;

        if (!IsPlayerOOB(posToMove, playerTall)) {
            ply1.positionY = posToMove;
        }

        posToMove = ply2.positionY;

        //Player2
        if (IsKeyDown(KEY_UP)) posToMove -= moveSpeed;
        if (IsKeyDown(KEY_DOWN)) posToMove += moveSpeed;
        //----------------------------------------------------------------------------------

        if (!IsPlayerOOB(posToMove, playerTall)) {
            ply2.positionY = posToMove;
        }
    }
    else {
        if (IsKeyPressed(KEY_M)) currentStatus = STATUS::MAIN_MENU;
    }

    if (IsKeyPressed(KEY_ENTER)) SetPaused(!pause);

}